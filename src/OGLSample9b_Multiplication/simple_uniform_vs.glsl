#version 430

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec2 vertex_texture_coordinates;
layout (std140, binding=1) uniform Mat {
   mat4 PVM;
};
layout (std140, binding=2) uniform Off {
   mat4 offsets[2];
};


out vec2 v_vertex_texture_coordinates;

void main()
{
    gl_Position = PVM * offsets[gl_InstanceID] * vec4(vertex_position + vec3(0, 0, 0.0), 1.0)  ;
    v_vertex_texture_coordinates = vertex_texture_coordinates;
}
