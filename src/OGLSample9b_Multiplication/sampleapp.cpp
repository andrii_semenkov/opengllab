#include "sampleapp.h"

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"

using namespace gl;

SampleApp::SampleApp() : OGLAppFramework::OGLApplication(1366u, 768u, "OGLSample 9b - Multiplication", 3u, 3u),
                         simple_program(0u), vbo_handle(0u), index_buffer_handle(0u), vao_handle(0u) {
}

bool SampleApp::init(void) {
    std::cout << "Init..." << std::endl;

    std::cout << "Shaders compilation..." << std::endl;

    // wczytanie z plikow i skompilowanie shaderow oraz utworzenie programu (VS + FS)
    std::string vs_path = "C:\\Users\\semenkov\\Desktop\\Grafika\\src\\OGLSample9b_Multiplication\\simple_uniform_vs.glsl";
    std::string fs_path = "C:\\Users\\semenkov\\Desktop\\Grafika\\src\\OGLSample9b_Multiplication\\fs_with_textures.glsl";

    if (auto create_program_result = OGLAppFramework::createProgram(vs_path, fs_path)) {
        simple_program = create_program_result.value();
    } else {
        std::cerr << "Error - can't create program..." << std::endl;
        return false;
    }

    // ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)

    // ustawienie programu, ktory bedzie uzywany podczas rysowania
    gl::glUseProgram(simple_program);

    unsigned int piramidesAmount = 2;
    glm::mat4 modelMatrices[2];

    for (unsigned int i = 0; i < piramidesAmount; i++) {
        glm::mat4 model = glm::mat4(1.0f);
        float rotAngle = (i * 180);
        model = glm::rotate(model, glm::radians(rotAngle), glm::vec3(0.0f, 0.0f, 1.f));
        model = glm::translate(model, glm::vec3(0, 0.3, 0));
        modelMatrices[i] = model;
    }
    unsigned int instanceVBO;
    glGenBuffers(1, &instanceVBO);
    glBindBuffer(GL_UNIFORM_BUFFER, instanceVBO);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(glm::mat4) * piramidesAmount, &modelMatrices[0], GL_STATIC_DRAW);
    glBindBufferBase(gl::GL_UNIFORM_BUFFER, piramidesAmount, instanceVBO);
    float vertices[] =
            {
                    //RED TRIANGLE
                    -0.5f, 0.f, 0.f, 0.5f, 0.191f,
                    0.f, 0.f, 0.5f, 0.191f, 0.5f,
                    0.f, 1.f, 0.f, 0.f, 0.f,
                    //GREEN TRIANGLE
                    0.f, 0.f, 0.5f, 0.191f, 0.5f,
                    0.5f, 0.f, 0.f, 0.5f, 0.809f,
                    0.f, 1.f, 0.f, 0.0f, 1.0f,
                    //BLUE TRIANGLE
                    -0.5f, 0.f, 0.f, 0.5f, 0.191f,
                    0.f, 0.f, -0.5f, 0.809f, 0.5f,
                    0.f, 1.f, 0.f, 1.0f, 0.0f,
                    //ORANGE TRIANGLE
                    0.5f, 0.f, 0.f, 0.5f, 0.809f,
                    0.f, 0.f, -0.5f, 0.809f, 0.5f,
                    0.f, 1.f, 0.f, 1.0f, 1.f,
                    //WHITE TRIANGLE
                    -0.5f, 0.f, 0.f, 0.191f, 0.5f,
                    0.5f, 0.f, 0.f, 0.809f, 0.5f,
                    0.f, 0.f, -0.5f, 0.5f, 0.191f,
                    //GREY TRIANGLE
                    -0.5f, 0.f, 0.f, 0.191f, 0.5f,
                    0.5f, 0.f, 0.f, 0.809f, 0.5f,
                    0.f, 0.f, 0.5f, 0.5f, 0.191f
            };


    unsigned int indices[] = {0, 1, piramidesAmount, 3, 4, 5, 8, 7, 6, 9, 10, 11, 14, 13, 12, 15, 16, 17};
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) 0);
    glEnableVertexAttribArray(0);
    // texture coord attribute
    glVertexAttribPointer(1, piramidesAmount, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *) (3 * sizeof(float)));
    glEnableVertexAttribArray(1);


    // load and create a texture
    // -------------------------
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    int width, height, nrChannels;
    unsigned char *data = stbi_load(
            "C:\\Users\\semenkov\\Desktop\\Grafika\\src\\OGLSample9b_Multiplication\\multicolor.png", &width, &height,
            &nrChannels, 0);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    } else {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);

    glm::mat4 m(1.0f);
    //LOOK FROM BELOW
    //glm::mat4 v = glm::lookAt(glm::vec3(0.f,-1.0f, 0.f), glm::vec3(0.f,0.f,0.f), glm::vec3(1.f, 0.f, 0.f));
    //LOOK AT FROM ABOVE
    //glm::mat4 v = glm::lookAt(glm::vec3(0.f,1.3f, 0.f), glm::vec3(0.f,0.f,0.f), glm::vec3(0.f, 0.f, 1.f));
    //LOOK FROM SIDE
    glm::mat4 v = glm::lookAt(glm::vec3(0.f, 0.f, 10.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f));
    //  glm::mat4 v = m;

    glm::mat4 p = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.f, 1.f);
    glm::mat4 mvp = p * v * m;
    gl::GLuint ubo_handle2(0u);
    gl::glGenBuffers(1, &ubo_handle2);
    gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, ubo_handle2);

    gl::glBufferData(gl::GL_UNIFORM_BUFFER, sizeof(glm::mat4), &mvp[0], gl::GL_STATIC_DRAW);
    gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 1, ubo_handle2);

    gl::glEnable(gl::GL_CULL_FACE);
    gl::glFrontFace(gl::GL_CCW);
    gl::glCullFace(gl::GL_BACK);
    return true;
}

bool SampleApp::frame(float delta_time) {
    gl::glDrawElementsInstanced(gl::GL_TRIANGLES, 18, gl::GL_UNSIGNED_INT, 0, tablesize);
    return true;
}

void SampleApp::release(void) {
    std::cout << "Release..." << std::endl;

    // odbindowanie VAO
    gl::glBindVertexArray(0);
    if (vao_handle) {
        // usuniecie VAO
        gl::glDeleteVertexArrays(1, &vao_handle);
        vao_handle = 0u;
    }

    // odbindowanie VBO
    gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
    if (vbo_handle) {
        // usuniecie VBO
        gl::glDeleteBuffers(1, &vbo_handle);
        vbo_handle = 0u;
    }

    // odbindowanie IB
    glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
    if (index_buffer_handle) {
        // usuniecie IB
        gl::glDeleteBuffers(1, &index_buffer_handle);
        index_buffer_handle = 0u;
    }

    // ustawienie aktywnego programu na 0 (zaden)
    gl::glUseProgram(0);

    // usuniecie programu
    gl::glDeleteProgram(simple_program);
}

bool SampleApp::prepareCommonPart() {

    return true;
}

void SampleApp::keyCallback(int key, int scancode, int action, int mods) {
    //std::cout << "Key pressed" << std::endl;
}

void SampleApp::cursorPosCallback(double xpos, double ypos) {
    //std::cout << "Cursor pos: " << xpos << ", " << ypos << std::endl;
}

void SampleApp::mouseButtonCallback(int button, int action, int mods) {
    //std::cout << "Mouse button pressed" << std::endl;
}

void SampleApp::reshapeCallback(std::uint16_t width, std::uint16_t height) {
    std::cout << "Reshape..." << std::endl;
    std::cout << "New window size: " << width << " x " << height << std::endl;

    gl::glViewport(0, 0, width, height);
}

