#include "sampleapp.h"

SampleApp::SampleApp() : OGLAppFramework::OGLApplication(1366u, 768u, "OGLSample12_Resize", 3u, 3u), simple_program(0u), vbo_handle(0u), index_buffer_handle(0u), vao_handle(0u)
{
    P = glm::mat4(0);
    //VM = glm::mat4(0);
}

SampleApp::~SampleApp()
{
}

void SampleApp::reshapeCallback(std::uint16_t width, std::uint16_t height)
{
   std::cout << "Reshape..." << std::endl;
   std::cout << "New window size: " << width << " x " << height << std::endl;

   gl::glViewport(0, 0, width, height);
   P = glm::perspective(3.14f / 3.0f, static_cast<float>(width) / static_cast<float>(height), 0.1f, 100.0f);

   glm::vec3 cameraPos = glm::vec3(2.f, 0.f, 1.f);
   glm::vec3 cameraTar = glm::vec3(0.f, 0.f, 0.f);
   glm::vec3  cameraUp = glm::vec3(0.f, 1.f, 0.f);
   glm::mat4 V = glm::lookAt(cameraPos, cameraTar, cameraUp);

   glm::mat4 M(1.0f);

   glm::mat4 VM = V * M;
   glm::mat4 PVM = P * VM;

   gl::GLuint ubo_transform(0u);
   gl::glGenBuffers(1, &ubo_transform);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, ubo_transform);
   gl::glBufferData(gl::GL_UNIFORM_BUFFER, 3 * 16 * sizeof(float), nullptr, gl::GL_STATIC_DRAW);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(0), 16 * sizeof(float)   , &PVM);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(16 * sizeof(float))      , 16 * sizeof(float), &VM);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(2 * 16 * sizeof(float))  , 12 * sizeof(float), &VM);
   gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 2, ubo_transform);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
}

void SampleApp::keyCallback(int key, int scancode, int action, int mods)
{
   //std::cout << "Key pressed" << std::endl;
}

void SampleApp::cursorPosCallback(double xpos, double ypos)
{
   //std::cout << "Cursor pos: " << xpos << ", " << ypos << std::endl;
}

void SampleApp::mouseButtonCallback(int button, int action, int mods)
{
   //std::cout << "Mouse button pressed" << std::endl;
}

bool SampleApp::init(void)
{
   std::cout << "Init..." << std::endl;
   // ustalamy domyślny kolor ekranu
   gl::glClearColor(0.25f, 0.25f, 0.25f, 1.f);


   std::cout << "Shaders compilation..." << std::endl;
   // wczytanie z plikow i skompilowanie shaderow oraz utworzenie programu (VS + FS)
   std::string vs_path = "C:\\Users\\semenkov\\Desktop\\Grafika\\shaders\\lsnienie\\very_simple_vs.glsl";
   std::string fs_path = "C:\\Users\\semenkov\\Desktop\\Grafika\\shaders\\lsnienie\\very_simple_fs.glsl";

   if (auto create_program_result = OGLAppFramework::createProgram(vs_path, fs_path))
   {
      simple_program = create_program_result.value();
      // ustawienie programu, ktory bedzie uzywany podczas rysowania
      gl::glUseProgram(simple_program);
   }
   else
   {
      std::cerr << "Error - can't create program..." << std::endl;
      return false;
   }

   // ustawienie informacji o lokalizacji atrybutu pozycji w vs (musi sie zgadzac z tym co mamy w VS!!!)
   const gl::GLuint vertex_position_loction = 0u;
   const gl::GLuint color_bind_point = 1u;
   const gl::GLuint normal_bind_point = 2u;

   // stworzenie tablicy z danymi o wierzcholkach 3x (x, y, z)		

   std::array<gl::GLfloat, 54u> vertices = {

      -1.f, 1.f, 0.f,
      1.f, 1.f, 1.f,
      0.f, 0.f, 1.f,

      -1.f, -1.f, 0.f,
      1.f, 1.f, 1.f,
      0.f, 0.f, 1.f,

      1.f, -1.f, 0.f,
      1.f, 1.f, 1.f,
      0.f, 0.f, 1.f,

      1.0f, -1.0f, 0.f,
      1.f, 0.f, 0.f,
      0.f, 0.f, 1.f,

      1.f, 1.f, 0.f,
      1.f, 0.f, 0.f,
      0.f, 0.f, 1.f,

      -1.f, 1.f, 0.f,
      1.f, 0.f, 0.f,
      0.f, 0.f, 1.f
   };

   // stworzenie tablicy z danymi o indeksach
   std::array<gl::GLushort, 6u> indices = { 0, 1, 2, 3, 4, 5 };

   std::cout << "Generating buffers..." << std::endl;
   // stworzenie bufora
   gl::glGenBuffers(1, &vbo_handle);
   // zbindowanie bufora jako VBO
   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo_handle);
   // alokacja pamieci dla bufora zbindowanego jako VBO i skopiowanie danych z tablicy "vertices"
   gl::glBufferData(gl::GL_ARRAY_BUFFER, vertices.size() * sizeof(gl::GLfloat), vertices.data(), gl::GL_STATIC_DRAW);
   // odbindowanie buffora zbindowanego jako VBO (zeby przypadkiem nie narobic sobie klopotow...)
   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);


   gl::GLuint ubo_handle(0u);
   gl::glGenBuffers(1, &ubo_handle);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, ubo_handle);

   gl::GLfloat intensity = 1;
   gl::glBufferData(gl::GL_UNIFORM_BUFFER, sizeof(float), &intensity, gl::GL_STATIC_DRAW);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);
   gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 0, ubo_handle);

   /*glm::mat4 */P = glm::perspective(3.14f / 3.0f, 16.0f / 9.0f, 0.1f, 100.0f);

   glm::vec3 cameraPos = glm::vec3(2.f, 0.f, 1.f);
   glm::vec3 cameraTar = glm::vec3(0.f, 0.f, 0.f);
   glm::vec3  cameraUp = glm::vec3(0.f, 1.f, 0.f);
   glm::mat4 V = glm::lookAt(cameraPos, cameraTar, cameraUp);

   glm::mat4 M(1.0f);

   glm::mat4 VM = V * M;
   glm::mat4 PVM = P * VM;

   gl::GLuint ubo_transform(0u);
   gl::glGenBuffers(1, &ubo_transform);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, ubo_transform);
   gl::glBufferData(gl::GL_UNIFORM_BUFFER, 3 * 16 * sizeof(float), nullptr, gl::GL_STATIC_DRAW);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(0), 16 * sizeof(float)   , &PVM);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(16 * sizeof(float))      , 16 * sizeof(float), &VM);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(2 * 16 * sizeof(float))  , 12 * sizeof(float), &VM);
   gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 2, ubo_transform);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);

   //================================================OSWIETLENIE================================================

   gl::GLuint light_handle(0u);
   gl::glGenBuffers(1, &light_handle);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, light_handle);

//   auto light_uniform_index = gl::glGetUniformBlockIndex(simple_program, "Light");
//   if (light_uniform_index == gl::GL_INVALID_INDEX)
//   {
//      std::cout << "Uniform Light not found\n";
//      return false;
//   }

//   gl::glUniformBlockBinding(simple_program, light_uniform_index, 2);

   glm::vec3 light_position_ = glm::vec3(0.f, 0.f, 0.2f);
   glm::vec4 light_position_view = V * glm::vec4(light_position_, 1);

   gl::glBufferData(gl::GL_UNIFORM_BUFFER, 4 * sizeof(float), nullptr, gl::GL_STATIC_DRAW);
   gl::glBufferSubData(gl::GL_UNIFORM_BUFFER, static_cast<gl::GLsizeiptr>(0), 3 * sizeof(float), &light_position_view);
   gl::glBindBufferBase(gl::GL_UNIFORM_BUFFER, 3, light_handle);
   gl::glBindBuffer(gl::GL_UNIFORM_BUFFER, 0);

   //================================================OSWIETLENIE================================================


   // stworzenie bufora
   gl::glGenBuffers(1, &index_buffer_handle);
   // zbindowanie bufora jako IB
   gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle);
   // alokacja pamieci dla bufora zbindowanego jako IB i skopiowanie danych z tablicy "indeices"
   gl::glBufferData(gl::GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(gl::GLushort), indices.data(), gl::GL_STATIC_DRAW);
   // odbindowanie buffora zbindowanego jako IB (zeby przypadkiem nie narobic sobie klopotow...)
   gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);

   // stworzenie VAO
   gl::glGenVertexArrays(1, &vao_handle);
   // zbindowanie VAO
   gl::glBindVertexArray(vao_handle);

   // zbindowanie VBO do aktualnego VAO
   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, vbo_handle);
   // ustalenie jak maja byc interpretowane dane z VBO
   gl::glVertexAttribPointer(vertex_position_loction, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 9, nullptr);  //vertex_position
   gl::glVertexAttribPointer(color_bind_point, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 9, reinterpret_cast<const void*>(3 * sizeof(float))); //vertex_color
   gl::glVertexAttribPointer(normal_bind_point, 3, gl::GL_FLOAT, gl::GL_FALSE, sizeof(float) * 9, reinterpret_cast<const void*>(6 * sizeof(float))); //vertex_normal
   // odblokowanie mozliwosci wczytywania danych z danej lokalizacji
   gl::glEnableVertexAttribArray(vertex_position_loction);
   gl::glEnableVertexAttribArray(color_bind_point);
   gl::glEnableVertexAttribArray(normal_bind_point);
   // zbindowanie IB do aktualnego VAO
   gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, index_buffer_handle);
   // odbindowanie VAO (ma ono teraz informacje m.in. o VBO + IB, wiec gdy zajdzie potrzeba uzycia VBO + IB, wystarczy zbindowac VAO)
   gl::glBindVertexArray(0u);

   // odbindowanie buffora zbindowanego jako VBO (zeby przypadkiem nie narobic sobie klopotow...)
   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
   // odbindowanie buffora zbindowanego jako bufor indeksow (zeby przypadkiem nie narobic sobie klopotow...)
   gl::glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);

   // na tym mozna zakonczyc
   // w tej prostej aplikacji bedziemy rysowac tylko 1 (powyzej stworzony) model, dlatego mozemy juz teraz ustawic odpowiednie dane dla naszych shaderow (nie beda sie one zmieniac co klatke...)
   // zeby narysowac nasz model musimy ustawic odpowiednie VBO + IB (a dzieki temu ze VAO ma o nich iformacje sprowadza sie to do ustawienia odpowiedniego VAO, a przez to reszte buforow)
   // Czyli znowu bindujemy VAO
   gl::glBindVertexArray(vao_handle);

   return true;
}

bool SampleApp::frame(float delta_time)
{
   // rozpoczynamy rysowanie uzywajac ustawionego programu (shader-ow) i ustawionych buforow
   gl::glDrawElements(gl::GL_TRIANGLES, 6, gl::GL_UNSIGNED_SHORT, nullptr);

   return true;
}

void SampleApp::release(void)
{
   std::cout << "Release..." << std::endl;

   // odbindowanie VAO
   gl::glBindVertexArray(0);
   if (vao_handle)
   {
      // usuniecie VAO
      gl::glDeleteVertexArrays(1, &vao_handle);
      vao_handle = 0u;
   }

   // odbindowanie VBO
   gl::glBindBuffer(gl::GL_ARRAY_BUFFER, 0);
   if (vbo_handle)
   {
      // usuniecie VBO
      gl::glDeleteBuffers(1, &vbo_handle);
      vbo_handle = 0u;
   }

   // odbindowanie IB
   glBindBuffer(gl::GL_ELEMENT_ARRAY_BUFFER, 0);
   if (index_buffer_handle)
   {
      // usuniecie IB
      gl::glDeleteBuffers(1, &index_buffer_handle);
      index_buffer_handle = 0u;
   }

   // ustawienie aktywnego programu na 0 (zaden)
   gl::glUseProgram(0);

   // usuniecie programu
   gl::glDeleteProgram(simple_program);
}
