#version 330
out vec4 vFragColor;
in  vec2 v_vertex_texture_coordinates;
uniform sampler2D color;
void main()
{
    vFragColor = texture(color, v_vertex_texture_coordinates);
}
