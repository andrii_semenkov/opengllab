#version 430

layout(location=0) in vec3 vertex_position;
layout(location=3) in vec3 vertex_color;
layout(std140, binding=1) uniform PVM {
    mat4 pvm;
};
out vec3 v_color;
void main()
{
    gl_Position = pvm * vec4(vertex_position, 1);
    v_color = vertex_color*1;
}
