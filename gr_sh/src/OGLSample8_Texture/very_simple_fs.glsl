#version 430

out vec4 frag_color;

in vec2 v_color;

uniform sampler2D color;

void main()
{
    frag_color = texture(color, v_color);
}
