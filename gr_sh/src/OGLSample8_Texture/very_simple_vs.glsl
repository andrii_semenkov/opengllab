#version 430

layout(location=0) in vec3 vertex_position;
layout(location=1) in vec2 vertex_color;

out vec2 v_color;

layout(std140, binding=1) uniform Intensity{
float intensity;
};

layout(std140, binding=2) uniform Transform{
mat4 T;
};

void main()
{
        v_color = vertex_color*intensity;
        gl_Position = T*vec4(vertex_position, 1.f);
}
