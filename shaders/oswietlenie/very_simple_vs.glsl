#version 430

layout(location=0) in vec3 vertex_position;
layout(location=1) in vec3 vertex_color;
layout(location=2) in vec3 vertex_normal;

out vec3 v_color;

out vec3 v_vertex_position_view;
out vec3 v_vertex_normal_view;

layout(std140, binding=0) uniform Intensity
{
   float intensity;
};

layout(std140, binding=2) uniform Transform
{
   mat4 PVM;
   mat4 VM;
   mat4 VM_norm;
};

void main()
{
   v_color = vertex_color*intensity;

   v_vertex_position_view = (VM*vec4(vertex_position, 1)).xyz;
   v_vertex_normal_view = (VM_norm*vec4(vertex_normal, 1)).xyz;

   gl_Position = PVM*vec4(vertex_position, 1.f);
}
