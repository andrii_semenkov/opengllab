#version 430

out vec4 frag_color;

in vec3 v_color;

uniform sampler2D color;

in vec3 v_vertex_normal_view;
in vec3 v_vertex_position_view;

layout(std140, binding=3) uniform Light {
    vec3 light_pos_view;
};

void main()
{
   vec3 light_pos = light_pos_view.xyz;
   vec3 light_vector = normalize(light_pos - v_vertex_position_view);
   float light_angle_cos = dot(normalize(v_vertex_normal_view), light_vector);

   vec3 light_color = v_color*light_angle_cos;

//    vec3 view = normalize(-v_vertex_position_view);
//    vec3 half_vec = normalize(light_vector + view);
//    float specAngle = max(dot(half_vec, v_vertex_normal_view), 0.f);
//    float spec = pow(specAngle, 500.0f);
   
   

   frag_color = vec4(light_color, 1.f);
}