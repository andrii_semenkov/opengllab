#version 430

layout(location=0) in vec3 vertex_position;
layout(location=3) in vec3 vertex_color;
layout(std140, binding=1) uniform Intensity {
   float intensity;
};
out vec3 v_color;
void main()
{
    gl_Position = vec4(vertex_position, 1.f);
    v_color = vertex_color*intensity;
}
