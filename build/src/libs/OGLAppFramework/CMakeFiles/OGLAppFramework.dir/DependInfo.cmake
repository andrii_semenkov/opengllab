# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/c/Users/semenkov/Desktop/Grafika/src/libs/OGLAppFramework/oglapplication.cpp" "/mnt/c/Users/semenkov/Desktop/Grafika/build/src/libs/OGLAppFramework/CMakeFiles/OGLAppFramework.dir/oglapplication.cpp.o"
  "/mnt/c/Users/semenkov/Desktop/Grafika/src/libs/OGLAppFramework/utilities.cpp" "/mnt/c/Users/semenkov/Desktop/Grafika/build/src/libs/OGLAppFramework/CMakeFiles/OGLAppFramework.dir/utilities.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../libs/glm/include"
  "../libs/gli/include"
  "../libs/glbinding/include"
  "../libs/glbinding/Linux/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
